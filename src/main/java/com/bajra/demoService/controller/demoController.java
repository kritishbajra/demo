package com.bajra.demoService.controller;

import com.bajra.demoService.entity.Users;
import com.bajra.demoService.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class demoController {
@Autowired
    private UserService service;
    @GetMapping("/home")
    public Users showMessage(){
        
        return service.getUsersDetails();

    }
}
